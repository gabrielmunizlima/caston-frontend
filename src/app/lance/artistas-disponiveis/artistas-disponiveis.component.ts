import { Component, EventEmitter, OnInit, Output, Inject } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource, MatDialog, MAT_DIALOG_DATA, MatDialogRef, MatDialogConfig } from '@angular/material';
import { ModalEnviarLanceComponent } from '../modal-enviar-lance/modal-enviar-lance.component';
import { Brand } from 'src/models/brand/brand';
import { Event } from 'src/models/event/Event';
import { AuctionService } from 'src/app/services/auction.service';
import { Offer } from 'src/models/auction/offer';
import { EventService } from 'src/app/services/event.service';
import { BrandService } from 'src/app/services/brand.service';
import { ScheduleService } from 'src/app/services/schedule.service';
import { PageSchedule } from 'src/models/event/page-schedule';
import { MarcasDetalhes } from 'src/app/marcas-detalhes/marcas-detalhes';
@Component({
    selector: 'app-artistas-disponiveis',
    templateUrl: './artistas-disponiveis.component.html',
    styleUrls: ['./artistas-disponiveis.component.scss']
})
export class ArtistasDisponiveisComponent implements OnInit {

    @Output() mudaComponente = new EventEmitter();

    displayedColumns: string[] = ['Nome', 'Gênero Principal', 'Distância até o Evento', 'Maior Oferta', 'Score', 'Dar Lance', 'SetList'];
    dataSource = new MatTableDataSource();
    selection = new SelectionModel<any>(true, []);
    expandedElement: null;

    brands = new Array<Brand>();
    event = new Event();
    event_id: number;

    constructor(
        private scheduleService: ScheduleService,
        public brandService: BrandService,
        private eventService: EventService,
        private auctionService: AuctionService,
        public matDialog: MatDialog,
        public dialogRef: MatDialogRef<MarcasDetalhes>,
        @Inject(MAT_DIALOG_DATA) public data
    ) {
        this.event_id = data;
    }

    ngOnInit() {
        this.getEventInfo(this.event_id);
    }

    handleSendOffer(brand: Brand) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.id = 'modal-enviar-lance-component';
        dialogConfig.height = '650px';
        dialogConfig.width = '700px';
        dialogConfig.data = {
            'brand': brand,
            'scheduleId': brand.scheduleId,
            'event': this.event
        };

        var dialog = this.matDialog.open(ModalEnviarLanceComponent, dialogConfig);
        dialog.afterClosed().subscribe(() => {
            this.auctionService.getMaxOfferByScheduleId(brand.scheduleId)
                .then((offer: Offer) => {
                    brand.minimumOffer = offer.value;
                })
                .catch(() => {
                    brand.minimumOffer = 0;
                });
        });
    }

    async getEventInfo(eventId: number) {
        await this.eventService.findEventById(eventId)
            .then((event: Event) => {
                this.event = event;
                this.getBrandAvailables(this.event);
            })
            .catch(() => {
                alert("Evento não encontrado!");
            });
    }


    async getBrandAvailables(event: Event) {
        this.brands = [];
        let scheduleList = await this.scheduleService.findScheduleAvailable(event.dthrInit, event.dthrFinal);
        // .then((scheduleList: PageSchedule) => {
        try {

            for (const value of scheduleList._embedded.scheduleList) {

                console.log("Schedule:");
                console.log(value);
                let brand = await this.brandService.findBrandById(value.brandId);
                console.log("Marca:");
                console.log(brand);
                brand.scheduleId = value.id;
                if (this.brands.find(b => b.id == brand.id) === undefined) {
                await this.auctionService.getMaxOfferByScheduleId(value.id)
                    .then((offer: Offer) => {
                        brand.minimumOffer = offer.value;
                    })
                    .catch(() => {
                        brand.minimumOffer = 0;
                    })
                    .finally(() => {
                        console.log("banda adicionada: total: " + this.brands.length);
                        if (this.brands.find(b => b.id == brand.id) === undefined) {
                            brand.principalGenre = brand.genres.map(_ => _.name).join();
                            //TODO: fix
                            brand.distanceEventKm = brand.members.length;

                            this.brands.push(brand);
                        }
                        //  this.dataSource = new MatTableDataSource(this.brands);
                    });
                }
            }

        }
        catch (err) {
            console.log(err);
            alert("Nenhuma Marca encontrada dentro do periodo deste evento! " + err);
        }
        finally {
            console.log("atualizando itens. Bandas localizadas: " + this.brands.length);
            this.dataSource = new MatTableDataSource(this.brands);
        };

    }

    async getBrandInfoById(brandId: number) {
        this.brandService.findBrandById(brandId)
            .then((brand: Brand) => {

                this.scheduleService.findScheduleByBrandId(brandId)
                    .then((scheduleList: PageSchedule) => {
                        brand.principalGenre = brand.genres[0].name;
                        brand.distanceEventKm = 10;
                        brand.scheduleId = scheduleList._embedded.scheduleList[0].id;                

                        this.auctionService.getMaxOfferByScheduleId(brand.scheduleId)
                            .then((offer: Offer) => {
                                brand.minimumOffer = offer.value;
                            })
                            .catch(() => {
                                brand.minimumOffer = 0;
                            })
                            .finally(() => {
                                this.brands.push(brand);
                                this.dataSource = new MatTableDataSource(this.brands);
                            });
                    })
                    .catch(() => {
                        alert("Agenda não encontrada!");
                    });
            })
            .catch(() => {
                alert("Marca não encontrada!");
            });
    }
}