import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistasDisponiveisComponent } from './artistas-disponiveis.component';

describe('ArtistasDisponiveisComponent', () => {
  let component: ArtistasDisponiveisComponent;
  let fixture: ComponentFixture<ArtistasDisponiveisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistasDisponiveisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistasDisponiveisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
