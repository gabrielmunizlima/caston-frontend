import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Event } from 'src/models/event/Event';
import { Offer } from 'src/models/auction/offer';
import { Address } from 'src/models/commons/Address';
import { AuctionService } from 'src/app/services/auction.service';
import { MatDialogRef } from '@angular/material';
import { Auction } from 'src/models/auction/auction';

@Component({
	selector: 'app-modal-lance-recebido',
	templateUrl: './modal-lance-recebido.component.html',
	styleUrls: ['./modal-lance-recebido.component.scss']
})
export class ModalLanceRecebidoComponent implements OnInit {

	auction: Auction = new Auction();
	offer: Offer = new Offer();
	event: Event = new Event();
	address: Address = new Address();
	auctionId: number = 0;

	constructor(
		private auctionService: AuctionService,
        public dialogRef: MatDialogRef<ModalLanceRecebidoComponent>,
		) { }

	ngOnInit() {
		this.auctionId = this.dialogRef._containerInstance._config.data.auctionId;
		this.event = this.dialogRef._containerInstance._config.data.event;
		this.address = this.event.eventplace.address;
		this.getAuctionInfo(this.auctionId);
	}

	handleUpdateOffer(action: boolean) {
		if(action == true) {
			var ok = confirm('Atenção!\nVocê está prestes a aceitar esse contrato de trabalho\n' + 
			'Você realmente quer aceitar essa oferta?');
		} else {
			var ok = confirm('Atenção!\nVocê está prestes a recusar esse contrato de trabalho\n' + 
			'Você realmente quer recusar essa oferta?');
		}

		if(ok == true) {
			this.offer.accept = action;
			this.updateOffer(this.offer);
		}
	}

	async getAuctionInfo(auctionId: number) {
        this.auctionService.findAuctionById(auctionId)
            .then((auction: Auction) => {
				this.auction = auction;
				this.offer = auction.offers[auction.offers.length -1];
            })
            .catch(() => {
                alert("Lance não encontrado!");
            });
    }

	async updateOffer(offer: Offer) {
        this.auctionService.updateOffer(this.auctionId, offer)
            .then(() => {
                alert('Sua ação foi registrada com sucesso');
                this.ngOnInit();
            })
            .catch(() => {
                alert("Não foi possível registar sua ação, tente novamente mais tarde!");
            });
    }
}