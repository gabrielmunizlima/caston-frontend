import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalLanceRecebidoComponent } from './modal-lance-recebido.component';

describe('LanceRecebidoComponent', () => {
  let component: ModalLanceRecebidoComponent;
  let fixture: ComponentFixture<ModalLanceRecebidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalLanceRecebidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalLanceRecebidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
