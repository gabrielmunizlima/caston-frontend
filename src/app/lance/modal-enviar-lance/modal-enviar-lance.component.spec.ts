import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEnviarLanceComponent } from './modal-enviar-lance.component';

describe('EnviarLanceComponent', () => {
  let component: ModalEnviarLanceComponent;
  let fixture: ComponentFixture<ModalEnviarLanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEnviarLanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEnviarLanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
