import { Component, OnInit } from '@angular/core';
import { Brand } from 'src/models/brand/brand';
import { Event } from 'src/models/event/Event';
import { Offer } from 'src/models/auction/offer';
import { AuctionService } from 'src/app/services/auction.service';
import { MatDialogRef } from '@angular/material';
import { Auction } from 'src/models/auction/auction';
import { AuctionStatus } from 'src/models/auction/auction-status';

@Component({
    selector: 'app-enviar-lance',
    templateUrl: './modal-enviar-lance.component.html',
    styleUrls: ['./modal-enviar-lance.component.scss']
})
export class ModalEnviarLanceComponent implements OnInit {

    brand: Brand;
    event: Event;

    highestOffer: number = 0;
    yourOffer: number = 0;

    newOffer: number;

    auctionId: number = 0;
    scheduleId: number = 0;

    constructor(
        private auctionService: AuctionService,
        public dialogRef: MatDialogRef<ModalEnviarLanceComponent>
    ) { }

    ngOnInit() {
        this.brand = this.dialogRef._containerInstance._config.data.brand;
        this.event = this.dialogRef._containerInstance._config.data.event;
        this.scheduleId = this.dialogRef._containerInstance._config.data.scheduleId;

        this.getMaxOfferByScheduleId(this.scheduleId);
    }

    closeModal() {
        this.dialogRef.close();
    }

    handleSendOffer() {
        if (this.newOffer <= 0) {
            alert('Valor inválido para lance!');
            return;
        } else if (this.newOffer <= this.highestOffer) {
            alert('Um novo lance deve sempre ser maior que o anterior!');
            return;
        } else {
            var offer = new Offer();
            offer.value = this.newOffer;

            if(this.auctionId > 0) {
                this.registerOffer(offer);
            } else {
                this.createAuction(offer);
            }
        }
    }

    async getMaxOfferByScheduleId(scheduleId: number) {
        this.auctionService.getMaxOfferByScheduleId(scheduleId)
            .then((offer: Offer) => {
                this.highestOffer = offer.value;
                this.getYourAuction(this.event.id, this.scheduleId);
            });
    }

    async createAuction(offer: Offer) {
        var auction = new Auction();
        auction.brandId = this.brand.id;
        auction.scheduleId = this.scheduleId;
        auction.eventId = this.event.id;
        auction.qtdOffers = 1;
        auction.auctionStatus = AuctionStatus.SENT;
        auction.offers = Array.of(offer);

        this.auctionService.registerAuction(auction)
            .then((auction: Auction) => {
                alert('Parabéns! Seu lance foi registrado com sucesso');
                this.auctionId = auction.id;
                this.ngOnInit();
            })
            .catch(() => {
                alert("Não foi possível registar seu lance, tente novamente mais tarde!");
            });
    }

    async registerOffer(offer: Offer) {
        this.auctionService.registerOffer(this.auctionId, offer)
            .then(() => {
                alert('Parabéns! Seu lance foi registrado com sucesso');
                this.ngOnInit();
            })
            .catch(() => {
                alert("Não foi possível registar seu lance, tente novamente mais tarde!");
            });
    }

    async getYourAuction(eventId: number, scheduleId: number) {
        this.auctionService.findAuctionByEventIdAndScheduleId(eventId, scheduleId)
            .then((auction: Auction) => {
                this.auctionId = auction.id;
                this.yourOffer = auction.offers[auction.offers.length -1].value;
            })
            .catch(() => {
                this.auctionId = 0;
                this.yourOffer = 0;
            });
    }
}