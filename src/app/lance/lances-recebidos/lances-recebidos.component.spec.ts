import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LancesRecebidosComponent } from './lances-recebidos.component';

describe('LancesRecebidosComponent', () => {
  let component: LancesRecebidosComponent;
  let fixture: ComponentFixture<LancesRecebidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LancesRecebidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LancesRecebidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
