import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { MatDialog, MatTableDataSource, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material';
import { AuctionService } from 'src/app/services/auction.service';
import { SelectionModel } from '@angular/cdk/collections';
import { Auction } from 'src/models/auction/auction';
import { ModalLanceRecebidoComponent } from '../modal-lance-recebido/modal-lance-recebido.component';
import { EventService } from 'src/app/services/event.service';
import { Event } from 'src/models/event/Event';
import { BrandService } from 'src/app/services/brand.service';
import { Brand } from 'src/models/brand/brand';
import { PageAuction } from 'src/models/auction/page-auction';
import { ScheduleService } from 'src/app/services/schedule.service';
import { PageSchedule } from 'src/models/event/page-schedule';

@Component({
    selector: 'app-lances-recebidos',
    templateUrl: './lances-recebidos.component.html',
    styleUrls: ['./lances-recebidos.component.scss']
})
export class LancesRecebidosComponent implements OnInit {

    @Output() mudaComponente = new EventEmitter();
    scheduleId: number = 0;
    brandId: number = 0;

    scheduleList = new Array();
    auctionList = new Array();
    brand = new Brand();

    displayedColumns: string[] = ['Nome do Evento', 'Local', 'Cidade', 'Horário', 'Último Lance', 'Lance Aceito', 'Visualizar Lance'];
    dataSource = new MatTableDataSource(this.auctionList);
    selection = new SelectionModel<any>(true, []);
    expandedElement: null;  

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: { brandId: number, type: string },
        private brandService: BrandService,
        private scheduleService: ScheduleService,
        private eventService: EventService,
        private auctionService: AuctionService,
        public matDialog: MatDialog
    ) {
        this.brandId = data.brandId;
    }

    ngOnInit() {
        this.getBrandInfoById(this.brandId);
        this.getSchedulesByBrandId(this.brandId);
    }

    handleChangeSchedule(scheduleId: number) {
        this.getAuctionsByScheduleId(scheduleId);
    }

    handleSeeOffer(auction: Auction) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.id = 'modal-lance-recebido-component';
        dialogConfig.height = '550px';
        dialogConfig.width = '700px';
        dialogConfig.data = {
            'auctionId': auction.id,
            'event': auction.event
        };

        var dialog = this.matDialog.open(ModalLanceRecebidoComponent, dialogConfig);
        dialog.afterClosed().subscribe(() => {
            this.getAuctionsByScheduleId(this.scheduleId);
        });
    }

    async getBrandInfoById(brandId: number) {
        this.brandService.findBrandById(brandId)
            .then((brand: Brand) => {              
                this.brand = brand;
            })
            .catch(() => {
                alert("Marca não encontrada!");
            });
    }

    async getSchedulesByBrandId(brandId: number) {
        this.scheduleService.findScheduleByBrandId(brandId)
            .then((scheduleList: PageSchedule) => {
                var newList = new Array();
                scheduleList._embedded.scheduleList.filter(
                    it => {
                        if(it.available == true) {
                            newList.push(it);
                        }
                    }
                );       
                
                this.scheduleList = newList;
            })
            .catch(() => {
                alert("Agenda não encontrada!");
                this.scheduleList = new Array();
            });
    }

    async getAuctionsByScheduleId(scheduleId: number) {
        this.auctionService.findAuctionsByScheduleId(scheduleId)
            .then((auctionList: PageAuction) => {
                
                auctionList._embedded.auctionList.forEach((auction: Auction) => {
                    auction.lastOffer = auction.offers[auction.offers.length -1];
                   
                    this.eventService.findEventById(auction.eventId)
                        .then((event: Event) => {
                            auction.event = event;

                            this.auctionList = auctionList._embedded.auctionList;
                            this.dataSource = new MatTableDataSource(this.auctionList);
                        })
                        .catch(() => {
                            alert("Evento não encontrado!");
                        });
                });        
            })
            .catch(() => {
                this.auctionList = new Array();
                this.dataSource = new MatTableDataSource(this.auctionList);
            });
    }
}
