import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {UtilitiesService} from '../../services/utilities.service';
import { Schedule } from 'src/models/event/schedule';
import { SessionService } from 'src/app/services/session.service';
import { ScheduleService } from 'src/app/services/schedule.service';

@Component({
	selector: 'app-modal-calendario',
	templateUrl: './modal-horario-livre.component.html',
	styleUrls: ['./modal-horario-livre.component.scss']
})
export class ModalHorarioLivreComponent implements OnInit {

	freeDay: string;
	startTime: string;
	endTime: string;
	brandId: number = 0;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data: { brandId: number, type: string },
		private session: SessionService,
		private scheduleService: ScheduleService,
		public dialogRef: MatDialogRef<ModalHorarioLivreComponent>
	) {
		this.brandId = data.brandId;
	 }

	ngOnInit() {
	
	}

	closeModal() {
		this.dialogRef.close();
	}

	saveFreeDay() {
		if (
			UtilitiesService.isValidDate(this.freeDay) &&
			UtilitiesService.isValidTime(this.startTime) &&
			UtilitiesService.isValidTime(this.endTime)
		) {
			const startHourArray = this.startTime.split(':');
			const endHourArray = this.endTime.split(':');

			const startDate = new Date(this.freeDay + "T12:00");
			const endDate = new Date(this.freeDay + "T12:00");

			startDate.setHours(Number(startHourArray[0]), Number(startHourArray[1]));
			endDate.setHours(Number(endHourArray[0]), Number(endHourArray[1]));

			if (startDate.getTime() >= endDate.getTime()) {
				alert('O horário de término não deve ser menor que o horário de início!');
				return;
			} else {
				const schedule = new Schedule();
				schedule.brandId = this.brandId; 
				schedule.dthrInit = startDate;
				schedule.dthrFinal = endDate;
				schedule.available = true;

				this.registerSchedule(schedule);
			}
		} else {
			alert('Dados inválidos!');
		}
	}

	async registerSchedule(schedule: Schedule) {
        this.scheduleService.registerSchedule(schedule)
            .then(() => {
                alert('Seu horário livre foi registrado com sucesso');
				this.closeModal();
            })
            .catch(() => {
                alert("Não foi possível registar seu horário livre, tente novamente mais tarde!");
            });
    }
}
