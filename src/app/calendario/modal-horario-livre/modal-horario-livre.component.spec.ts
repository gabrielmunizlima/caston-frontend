import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalHorarioLivreComponent } from './modal-horario-livre.component';

describe('ModalCalendarioComponent', () => {
  let component: ModalHorarioLivreComponent;
  let fixture: ComponentFixture<ModalHorarioLivreComponent>;

  beforeEach(async(() => {
	TestBed.configureTestingModule({
		declarations: [ ModalHorarioLivreComponent ]
	})
	.compileComponents();
  }));

  beforeEach(() => {
	fixture = TestBed.createComponent(ModalHorarioLivreComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
  });

  it('should create', () => {
	expect(component).toBeTruthy();
  });
});
