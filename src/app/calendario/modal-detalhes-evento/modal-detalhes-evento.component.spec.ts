import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetalhesEventoComponent } from './modal-detalhes-evento.component';

describe('ModalDetalhesEventoComponent', () => {
  let component: ModalDetalhesEventoComponent;
  let fixture: ComponentFixture<ModalDetalhesEventoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDetalhesEventoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDetalhesEventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
