import {Component, OnInit} from '@angular/core';
import {Event} from '../../../models/event/Event';
import {MatDialogRef} from '@angular/material';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-modal-detalhes-evento',
  templateUrl: './modal-detalhes-evento.component.html',
  styleUrls: ['./modal-detalhes-evento.component.scss']
})
export class ModalDetalhesEventoComponent implements OnInit {

	event: Event;

	constructor(
		private eventService: EventService,
        public dialogRef: MatDialogRef<ModalDetalhesEventoComponent>,
		) { }

	ngOnInit() {
		const eventId = this.dialogRef._containerInstance._config.data.eventId;
		this.getEventInfo(eventId);
	}

	closeModal() {
		this.dialogRef.close();
	}

	async getEventInfo(eventId: number) {
        await this.eventService.findEventById(eventId)
            .then((event: Event) => {
                this.event = event;
            })
            .catch(() => {
                alert("Evento não encontrado!");
            });
    }
}
