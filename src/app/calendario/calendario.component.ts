import {Component, EventEmitter, OnInit, Output, Inject} from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import ptBr from '@fullcalendar/core/locales/pt-br';
import {MatDialog, MatDialogConfig, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ModalHorarioLivreComponent} from './modal-horario-livre/modal-horario-livre.component';
import {Event} from '../../models/event/Event';
import {EventStatus} from '../../models/event/eventstatus';
import {ModalDetalhesEventoComponent} from './modal-detalhes-evento/modal-detalhes-evento.component';
import { ScheduleService } from '../services/schedule.service';
import { EventService } from '../services/event.service';
import { PageEvent } from 'src/models/event/page-event';
import { PageSchedule } from 'src/models/event/page-schedule';

@Component({
	selector: 'app-calendario',
	templateUrl: './calendario.component.html',
	styleUrls: ['./calendario.component.scss']
})

export class CalendarioComponent implements OnInit {

	@Output() mudaComponente = new EventEmitter();

	calendarPlugins = [dayGridPlugin, interactionPlugin];
	calendarOptions =  {
		weekday: 'long',
		year: 'numeric',
		month: 'long',
		day: 'numeric'
	};

	locale = ptBr;
	brandId: number = 0;
	events = [];
	
    constructor(
		@Inject(MAT_DIALOG_DATA) public data: { brandId: number, type: string },
		public dialogRef: MatDialogRef<CalendarioComponent>,
		private eventService: EventService,
		private scheduleService: ScheduleService,
        public matDialog: MatDialog
    ) {
        this.brandId = data.brandId;
    }

	ngOnInit() {
		this.events = new Array();		
		this.getEventsByBrandId(this.brandId);
		this.getSchedulesByBrandId(this.brandId);
	}

    async getEventsByBrandId(brandId: number) {
        this.eventService.findAllEventsByBrandId(brandId)
            .then((eventList: PageEvent) => {
                var newList = new Array();
                eventList._embedded.eventList.filter(
                    it => {
                        if(it.bookingBy.brandId == brandId) {					
							newList.push(this.mountCalendarEvent(it));					
                        }
                    }
                );       
                
                this.events = this.events.concat(newList);
            })
            .catch(() => {
                alert("Agenda não encontrada!");
            });
	}
	
	async getSchedulesByBrandId(brandId: number) {
        this.scheduleService.findScheduleByBrandId(brandId)
            .then((scheduleList: PageSchedule) => {
                var newList = new Array();
                scheduleList._embedded.scheduleList.filter(
                    it => {
                        if(it.available == true) {
							const event = new Event();
							event.id = 0;
							event.name = 'Horário Livre';
							event.dthrInit = it.dthrInit;
							event.dthrFinal = it.dthrFinal;
							event.statusEvent = EventStatus.FREE_TIME;

							newList.push(this.mountCalendarEvent(event))
                        }
                    }
                );       
                
                this.events = this.events.concat(newList);
            })
            .catch(() => {
                alert("Agenda não encontrada!");
            });
	}
	
	addFreeDay() {
		const dialogConfig = new MatDialogConfig();
		dialogConfig.disableClose = true;
		dialogConfig.id = 'modal-horario-livre-component';
		dialogConfig.height = '400px';
		dialogConfig.width = '600px';
		dialogConfig.data = {
            'brandId': this.brandId
        };

		var dialog = this.matDialog.open(ModalHorarioLivreComponent, dialogConfig);
        dialog.afterClosed().subscribe(() => {
            this.ngOnInit();
        });
	}
	
	handleClickEvent(arg) {
		if(arg.event.id === '0') return;
		
		const dialogConfig = new MatDialogConfig();
		dialogConfig.disableClose = true;
		dialogConfig.id = 'modal-detalhes-evento-component';
		dialogConfig.height = '600px';
		dialogConfig.width = '700px';
		dialogConfig.data = {
            'eventId': arg.event.id
        };

		this.matDialog.open(ModalDetalhesEventoComponent, dialogConfig);
	}

	handleMouseOver(info) {
		var status = info.event.extendedProps.status;
		var backgroundColor = this.getEventColor(status);

		var div = document.createElement("DIV");   
		div.innerHTML = `<div style="background-color: ${backgroundColor}; color: white; margin: 1px 2px 0; padding: 0 1px; border-radius: 3px; border: 1px solid black;">
		<strong> Evento ${status} </strong> 
		<br/>
		Clique para detalhes 
		<br/>
		</div>`;   

	}

	mountCalendarEvent(event: Event) {
		return {
			id: event.id.toString(),
			title: event.name,
			start: event.dthrInit,
			end: event.dthrFinal,
			color: this.getEventColor(event.statusEvent),
			hour: 'numeric',
			minute: '2-digit',
			borderColor: 'black',
			status: event.statusEvent
		}
	}

	getEventColor(status: EventStatus){
		switch (status) {
			case EventStatus.OPEN:
				return '#d55a00';
			case EventStatus.READY:
				return	'#039103';
			case EventStatus.INPROGRESS:
				return '#12A0CF';
			case EventStatus.CANCELLED:
				return '#A91C1C';
			case EventStatus.REALIZED:
				return '#4b5948';
			case EventStatus.FREE_TIME:
				return '#1B7307';
		}
	}

	closeModal() {
		this.dialogRef.close();
	}

}