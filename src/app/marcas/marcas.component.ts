import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { MarcasDetalhes } from '../marcas-detalhes/marcas-detalhes';
import { BrandService } from '../services/brand.service';
import { Brand } from 'src/models/brand/brand';

@Component({
	selector: 'app-marcas',
	templateUrl: './marcas.component.html',
	styleUrls: ['./marcas.component.scss']
})
export class MarcasComponent implements OnInit {

	@Output() mudaComponente = new EventEmitter();

	displayedColumns: string[] = ['Nome', 'Fundador', 'Nº Participantes', 'Detalhes'];
	dataSource = new MatTableDataSource();
	selection = new SelectionModel<any>(true, []);
	expandedElement: null;

	brands: Brand[];

	constructor(
		private brandService: BrandService,
		public dialog: MatDialog
	) { }

	ngOnInit() {
		this.getBrands();
	}

	private async getBrands() {
		this.brands = await this.brandService.findAllBrands();
		this.dataSource = new MatTableDataSource(this.brands);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	adicionar() {
		this.mudaComponente.emit('marcas/adicionar');
	}

	onDetailButtonClick(element: Brand): void {
		const dialogRef = this.dialog.open(MarcasDetalhes, {
			width: '800px',
			data: { brand: element, showButtons: true }
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}
}
