import { Address } from './Address';

export interface Artist {
    Id: number;
    Name: string;
    Surname: string;
    Birthday: string;
    Address: Address;
    Telephone: string;
    Email: string;
}