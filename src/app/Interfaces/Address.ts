export interface Address {
    CEP: string;
    Street: string;
    N: string;
    Complemento: string;
    District: string;
    City: string;
    State: string;
    Country: string;
}