import { Score } from 'src/models/brand/score';
import { Artist } from './Artist';
import { Gender } from './Gender';
import { Member } from './Member';

export interface Band {
    id: number;
    name: string;
    owner: Artist;
    dateFoundation: string;
    members: Member[];
    approvals: Member[];
    genres: Gender[];
    minimumOffer: number;
    score: Score;
    setList: string[];
}