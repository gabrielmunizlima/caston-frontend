import { Address } from './Address';
import { Role } from './Role';

export interface Member {
    Id: number;
    Name: string;
    Surname: string;
    Birthday: string;
    Address: Address;
    Telephone: string;
    Email: string;
    Role: Role;
    Participation: number;
    Approved: boolean;
}