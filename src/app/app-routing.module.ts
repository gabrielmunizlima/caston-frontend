import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { AdicionaArtistaComponent } from './adiciona-artista/adiciona-artista.component';
import { ArtistasComponent } from './artistas/artistas.component';

import { DetalhesEventoComponent } from './evento/detalhes-evento/detalhes-evento.component';


import { SignupComponent } from './signup/signup.component';
import { MarcasComponent } from './marcas/marcas.component';
import { AdicionaMarcaComponent } from './adiciona-marca/adiciona-marca.component';

import { ArtistasDisponiveisComponent } from './lance/artistas-disponiveis/artistas-disponiveis.component';
import { LancesRecebidosComponent } from './lance/lances-recebidos/lances-recebidos.component';
import { ModalEnviarLanceComponent } from './lance/modal-enviar-lance/modal-enviar-lance.component';
import { ModalLanceRecebidoComponent } from './lance/modal-lance-recebido/modal-lance-recebido.component';
import { MeusEventosComponent } from './evento/meus-eventos/meus-eventos.component';
import { MinhasMarcasComponent } from './minhas-marcas/minhas-marcas.component';
import { AdicionarLocaisComponent } from './adicionar-locais/adicionar-locais.component';
import { AdicionarEventoComponent } from './evento/adicionar-evento/adicionar-evento.component';
import { CalendarioComponent } from './calendario/calendario.component';

const routes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'main', component: MainComponent },
	{ path: 'calendario', component: CalendarioComponent },
	{ path: 'artistas/adicionar', component: AdicionaArtistaComponent },
	{ path: 'evento/meus-eventos', component: MeusEventosComponent },
	{ path: 'evento/adicionar-evento' , component:AdicionarEventoComponent},
	{ path: 'lance/artistas-disponiveis', component: ArtistasDisponiveisComponent },
	{ path: 'lance/modal-enviar-lance', component: ModalEnviarLanceComponent },
	{ path: 'lance/lances-recebidos', component: LancesRecebidosComponent },
	{ path: 'lance/modal-lance-recebido', component: ModalLanceRecebidoComponent },
	{ path: 'artistas', component: ArtistasComponent },
	{ path: 'signup', component: SignupComponent },
	{ path: 'marcas', component: MarcasComponent },
	{ path: 'marcas/adicionar', component: AdicionaMarcaComponent },
	{ path: 'marcas/minhas', component: MinhasMarcasComponent },
	{ path: 'adicionarlocais', component:AdicionarLocaisComponent},
	{ path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {useHash: true})],
	exports: [RouterModule]
})
export class AppRoutingModule { }
