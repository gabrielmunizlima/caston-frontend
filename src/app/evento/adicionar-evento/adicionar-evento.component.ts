import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { AdicionarLocaisComponent } from 'src/app/adicionar-locais/adicionar-locais.component';
import { EventPlaceService } from 'src/app/services/event-place.service';
import { SessionService } from 'src/app/services/session.service';
import { Event } from 'src/models/event/Event';
import { EventPlace } from 'src/models/event/eventPlace';
import { EventService } from 'src/app/services/event.service';

interface Local {
	value: string;
	viewValue: string
}

@Component({
	selector: 'app-adicionar-evento',
	templateUrl: './adicionar-evento.component.html',
	styleUrls: ['./adicionar-evento.component.scss']
})
export class AdicionarEventoComponent implements OnInit {

	public Event: Event;
	EventPlaces = new Array<EventPlace>()
	selectedEventPlace: string;
	constructor(
		private notifierService: NotifierService,
		public dialogRef: MatDialogRef<AdicionarEventoComponent>,
		public dialog: MatDialog, 
		private eventPlaceService: EventPlaceService,
		private sessionService: SessionService,
		private eventService: EventService) { }


	ngOnInit() {
		this.Event = new Event();
		this.getEventPlaces();
	}

	openLocais(): void {
		const dialogRef = this.dialog.open(AdicionarLocaisComponent, {
			width: '70%'
		});

		dialogRef.afterClosed().subscribe(result => {
			this.getEventPlaces();
		});
	}

	cancel(): void {
		this.dialogRef.close();
	}

	async adicionarCalendario() {
		this.limpaCampos();
		this.notifierService.notify('success', 'Evento cadastrado com sucesso!');
	}

	limpaCampos() {
		this.Event = new Event();
		this.Event.eventplace = new EventPlace();
	}

	async getEventPlaces() {
		this.EventPlaces = await this.eventPlaceService.getByOwnerHirer(this.sessionService.idUsuario);
	}

	eventSelection(event) {
		this.selectedEventPlace = event.id;
	}

	onAddEventButtonClicked() {
		var result = this.eventService.save(this.Event);
	}
	
	onEventSelectChanged(id) {
		this.Event.eventplace = this.EventPlaces.filter(_ => _.id == id)[0];
	}
}
