import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { AdicionarEventoComponent } from '../adicionar-evento/adicionar-evento.component';
import { EventService } from 'src/app/services/event.service';
import { EventGrid } from '../../../models/event/eventGrid';
import { DetalhesEventoComponent } from '../detalhes-evento/detalhes-evento.component';
import { Event } from '../../../models/event/Event';
import { SessionService } from 'src/app/services/session.service';
import { EventStatus } from 'src/models/event/eventstatus';

@Component({
	selector: 'app-meus-eventos',
	templateUrl: './meus-eventos.component.html',
	styleUrls: ['./meus-eventos.component.scss']
})
export class MeusEventosComponent implements OnInit {

	@Output() mudaComponente = new EventEmitter();

	displayedColumns: string[] = ['Nome', 'Data Inicio', 'Data Fim', 'Situacao', 'Detalhes'];
	dataSource = new MatTableDataSource();
	selection = new SelectionModel<any>(true, []);
	expandedElement: null;

	Events = new Array<EventGrid>();
	EventsList: Event[];

	constructor(private router: Router, public dialog: MatDialog, private eventService: EventService, private sessionService: SessionService) { }

	ngOnInit() {
		this.getEvents();
	}

	openDialog(): void {
		const dialogRef = this.dialog.open(AdicionarEventoComponent, {
			width: '70%'
		});

		dialogRef.afterClosed().subscribe(() => {
			this.getEvents();
		});
	}

	onOpenDetailsButtonClick(eventGrid: Event) {
		var event = this.EventsList.filter(_ => _.id == eventGrid.id);
		if (event.length == 0) return; 

		const dialogRef = this.dialog.open(DetalhesEventoComponent, {
			width: '1000px',
			data: event[0]
		});

		dialogRef.afterClosed().subscribe(result => {
			if (!result) return;
			this.mudaComponente.emit('lance/artistas-disponiveis');
		});
	}

	public async getEvents() {
		this.Events = [];
		this.EventsList = await this.eventService.getEventsByOwnerHirer(this.sessionService.idUsuario);
		this.EventsList.forEach((item) => {
			this.Events.push(
				new EventGrid(
					item.id, 
					item.name, 
					item.dthrInit, 
					item.dthrFinal, 
					this.getStatusEventLabel(item.statusEvent))
			);
		});

		this.dataSource = new MatTableDataSource(this.Events);
	}

	getStatusEventLabel(status: string){
		switch (status) {
			case 'OPEN':
				return 'EM ABERTO';
			case 'READY':
				return	'PRONTO';
			case 'INPROGRESS':
				return 'EM ANDAMENTO';
			case 'CANCELLED':
				return 'CANCELADO';
			case 'REALIZED':
				return 'REALIZADO';
		}
	}
}


