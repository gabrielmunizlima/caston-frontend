import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { Event } from '../../../models/event/Event';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MarcasDetalhes } from 'src/app/marcas-detalhes/marcas-detalhes';
import { ArtistasDisponiveisComponent } from '../../lance/artistas-disponiveis/artistas-disponiveis.component';
import { BrandService } from 'src/app/services/brand.service';

@Component({
	selector: 'app-detalhes-evento',
	templateUrl: './detalhes-evento.component.html',
	styleUrls: ['./detalhes-evento.component.scss']
})
export class DetalhesEventoComponent implements OnInit {

	@Output() mudaComponente = new EventEmitter();
	componenteSelecionado = 'evento/detalhes-evento';

	Event: Event;

	constructor(public dialog: MatDialog,
		public dialogRef: MatDialogRef<MarcasDetalhes>,
		public brandService: BrandService,
		@Inject(MAT_DIALOG_DATA) public data) {
		this.Event = data;
	}

	ngOnInit() {}

	onSearchArtistButtonClick() {
		//this.dialogRef.close('lance/artistas-disponiveis');

		const dialogRef = this.dialog.open(ArtistasDisponiveisComponent, {
			width: '900px',
			height: '600px',
			data: this.Event.id
		});

		dialogRef.afterClosed().subscribe(result => {
			if (!result) return;
			this.mudaComponente.emit('lance/artistas-disponiveis');
		});

	}

	public onCloseButtonClick(): void {
		this.dialogRef.close();
	}

	public async onDetailBandButtonClick() {
		var brand = await this.brandService.findBrandById(this.Event.bookingBy.brandId);

		const dialogRef = this.dialog.open(MarcasDetalhes, {
			width: '800px',
			data: {brand: brand, showButtons: false}
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}
}