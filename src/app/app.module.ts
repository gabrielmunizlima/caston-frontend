import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyMaskConfig, CurrencyMaskModule, CURRENCY_MASK_CONFIG } from 'ng2-currency-mask';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MenuComponent } from './menu/menu.component';
import { TopperComponent } from './topper/topper.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoMaterialModule } from './material-modules';
import { HttpClientModule } from '@angular/common/http';
import { CalendarioComponent } from './calendario/calendario.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { FileUploadModule } from 'ng2-file-upload';
import { NgBrazil } from 'ng-brazil' 
import { TextMaskModule } from 'angular2-text-mask';

import { NotifierModule } from 'angular-notifier';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { AdicionaArtistaComponent } from './adiciona-artista/adiciona-artista.component';
import { AdicionaContratanteComponent } from './adiciona-contratante/adiciona-contratante.component';
import { ArtistasComponent } from './artistas/artistas.component';

import { DetalhesEventoComponent } from './evento/detalhes-evento/detalhes-evento.component';
import { ArtistasDisponiveisComponent } from './lance/artistas-disponiveis/artistas-disponiveis.component';
import { ModalEnviarLanceComponent } from './lance/modal-enviar-lance/modal-enviar-lance.component';
import { LancesRecebidosComponent } from './lance/lances-recebidos/lances-recebidos.component';

import { MarcasComponent } from './marcas/marcas.component';
import { AdicionaMarcaComponent } from './adiciona-marca/adiciona-marca.component';
import { SignupComponent } from './signup/signup.component';
import { ModalLanceRecebidoComponent } from './lance/modal-lance-recebido/modal-lance-recebido.component';
import { MeusEventosComponent } from './evento/meus-eventos/meus-eventos.component';
import { MinhasMarcasComponent } from './minhas-marcas/minhas-marcas.component';
import { MinhasMarcasDetalhes } from './minhas-marcas-detalhes/minhas-marcas-detalhes';

import { AdicionarLocaisComponent } from './adicionar-locais/adicionar-locais.component';
import { AdicionarEventoComponent } from './evento/adicionar-evento/adicionar-evento.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { ModalHorarioLivreComponent } from './calendario/modal-horario-livre/modal-horario-livre.component';
import { ModalDetalhesEventoComponent } from './calendario/modal-detalhes-evento/modal-detalhes-evento.component';
import { MarcasDetalhes } from './marcas-detalhes/marcas-detalhes';
import { CookieService } from 'ngx-cookie-service';
import { ConfirmationModal } from './confirmation-modal/confirmation-modal';

export let options: Partial<IConfig> | (() => Partial<IConfig>) = {};

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
	align: 'right',
	allowNegative: true,
	decimal: ',',
	precision: 2,
	prefix: 'R$ ',
	suffix: '',
	thousands: '.'
};

@NgModule({
	declarations: [
		AppComponent,
		DashboardComponent,
		MenuComponent,
		TopperComponent,
		CalendarioComponent,
		LoginComponent,
		MainComponent,
		AdicionaArtistaComponent,
		ArtistasComponent,
		DetalhesEventoComponent,
		ArtistasDisponiveisComponent,
		ModalEnviarLanceComponent,
		LancesRecebidosComponent,
		AdicionaContratanteComponent,
		SignupComponent,
		ArtistasComponent,
		MarcasComponent,
		MarcasDetalhes,
		MinhasMarcasComponent,
		AdicionaMarcaComponent,
		ModalLanceRecebidoComponent,
		MeusEventosComponent,
		MinhasMarcasDetalhes,
		AdicionarLocaisComponent,
		AdicionarEventoComponent,
		ConfirmationModal,
		AdicionarEventoComponent,
		ModalHorarioLivreComponent,
		ModalDetalhesEventoComponent

	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		CommonModule,
		BrowserModule,
		CurrencyMaskModule,
		AppRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		DemoMaterialModule,
		NgbModule,
		NgBrazil,
		TextMaskModule,
		HttpClientModule,
		FullCalendarModule,
		FileUploadModule,
		MatDialogModule,
		MatButtonModule,
		NotifierModule.withConfig({
			position: {
				horizontal: {
					position: 'right',
					distance: 12
				},
				vertical: {
					position: 'top',
				}
			}
		}
		),
		NgxMaskModule.forRoot(options)

	],
	entryComponents: [
		MarcasDetalhes,
		MinhasMarcasDetalhes,
		ConfirmationModal,
		ModalHorarioLivreComponent,
		ModalDetalhesEventoComponent,
		ModalEnviarLanceComponent,
		ModalLanceRecebidoComponent,
		DetalhesEventoComponent,
		CalendarioComponent
	],
	exports: [
		DemoMaterialModule
	],
	providers: [
		CookieService,
		{ provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
	],
	bootstrap: [AppComponent]
})
export class AppModule { }

