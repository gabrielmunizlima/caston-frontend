import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AdicionarEventoComponent } from '../evento/adicionar-evento/adicionar-evento.component';
import { LoginService } from '../services/login.service';
declare var $: any;

@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

	@Input() selectedComponent: string;
	@Output() mudaComponente = new EventEmitter();
	
	public typeUser: string;
	public menu: any;

	constructor(public dialog: MatDialog, public loginService: LoginService) {
		this.typeUser = loginService.type;
	}

	ngOnInit() {
		this.menu = [{
			id: 'dashboard',
			label: 'Dashboard'
		},
		{
			divider: true
		}
		/*,
		{
			id: 'artistas',
			label: 'Artistas'
		},
		{
			id: 'calendario',
			label: 'Calendario'
		}*/
		];
		if (this.typeUser == 'A') {
			this.menu.push({
				id: 'marcas',
				label: 'Marcas'
			});
			this.menu.push(
				{
					id: 'marcas/minhas',
					label: 'Minhas marcas'
				});
		} else if (this.typeUser == 'C') {

		}

		$('#sidebarToggle, #sidebarToggleTop').on('click', (e) => {
			$('body').toggleClass('sidebar-toggled');
			$('.sidebar').toggleClass('toggled');
			if ($('.sidebar').hasClass('toggled')) {
				$('.sidebar .collapse').collapse('hide');
			}
		});

		// Close any open menu accordions when window is resized below 768px
		$(window).resize(() => {
			if ($(window).width() < 768) {
				$('.sidebar .collapse').collapse('hide');
			}
		});

		// Prevent the content wrapper from scrolling when the fixed side navigation hovered over
		$('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', (e) => {
			if ($(window).width() > 768) {
				const e0 = e.originalEvent;
				const delta = e0.wheelDelta || -e0.detail;
				// this.scrollTop += (delta < 0 ? 1 : -1) * 30;
				e.preventDefault();
			}
		});

		// Scroll to top button appear
		$(document).on('scroll', () => {
			const scrollDistance = $(this).scrollTop();
			if (scrollDistance > 100) {
				$('.scroll-to-top').fadeIn();
			} else {
				$('.scroll-to-top').fadeOut();
			}
		});

		// Smooth scrolling using jQuery easing
		$(document).on('click', 'a.scroll-to-top', (e) => {
			const $anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: ($($anchor.attr('href')).offset().top)
			}, 1000, 'easeInOutExpo');
			e.preventDefault();
		});
	}

	openDialog(): void {
		const dialogRef = this.dialog.open(AdicionarEventoComponent, {
			width: '70%'
		});

		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed');
		});
	}

	mudarComponente(componente) {
		this.selectedComponent = componente;
		this.mudaComponente.emit(componente);
	}


}
