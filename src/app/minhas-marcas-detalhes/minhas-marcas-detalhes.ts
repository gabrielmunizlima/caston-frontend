import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationModal } from '../confirmation-modal/confirmation-modal';
import { Genre } from 'src/models/brand/genre';
import { Router } from '@angular/router';
import { BrandService } from 'src/app/services/brand.service';
import { LancesRecebidosComponent } from '../lance/lances-recebidos/lances-recebidos.component';
import { CalendarioComponent } from '../calendario/calendario.component';

@Component({
	selector: 'minhas-marcas-detalhes',
	templateUrl: 'minhas-marcas-detalhes.html',
	styleUrls: ['./minhas-marcas-detalhes.scss']

})
export class MinhasMarcasDetalhes {

	@Output() mudaComponente = new EventEmitter();
	componenteSelecionado = 'minhas-marcas-detalhes/minhas-marcas-detalhes';

	public displayedColumns: string[] = ['Nome', 'Email', 'Opções'];
	public dataSourceMembers = new MatTableDataSource();
	public dataSourceApproval = new MatTableDataSource();
	public selection = new SelectionModel<any>(true, []);

	public brand;
	public brandId: number;
	genre = new Genre();

	public Type: string;
	public Genres = [];
	public Approvals = [];

	constructor(
		private brandService: BrandService,
		public dialog: MatDialog,
		private router: Router,
		public dialogRef: MatDialogRef<MinhasMarcasDetalhes>,
		@Inject(MAT_DIALOG_DATA) public data: { brandId: number, type: string }
	) {
		this.brandId = data.brandId;
		this.Type = data.type;
	}


	ngOnInit() {
		this.getBrandInfoById(this.brandId);
	}

	public async getBrandInfoById(brandId: number) {
		this.brand = await this.brandService.findBrandById(brandId);
		this.genre = this.brand.genres[0];
		this.dataSourceMembers = new MatTableDataSource(this.brand.members);
		this.Genres = this.brand.genres;
		this.Approvals = [
			{
				id: 31,
				name: 'Rita de Moura',
				email: 'rita_moura90@gmail.com',
				Approved: false
			}
			/*,
			{
				id: 30,
				name: 'Carlos Almeida',
				email: 'carlos-almeida3030@gmail.com',
				Approved: false
			}*/
		];
		this.dataSourceApproval = new MatTableDataSource(this.Approvals);

	}

	public onRemoveMemberButtonClick(item) {
		const dialogRef = this.dialog.open(ConfirmationModal, {
			width: '300px',
			data: {
				message: "Deseja realmente remover o(a) " + item.Name + " da marca?",
				confirmMessage: "Sim",
				cancelMessage: "Não"
			}
		});

		dialogRef.afterClosed().subscribe(result => { });
	}

	public onApproveMemberButtonClick(item) {
		const dialogRef = this.dialog.open(ConfirmationModal, {
			width: '300px',
			data: {
				message: "Deseja realmente aprovar o(a) " + item.Name + " para participar da marca?",
				confirmMessage: "Sim",
				cancelMessage: "Não"
			}
		});

		dialogRef.afterClosed().subscribe(result => { });
	}

	public onDisapproveMemberButtonClick(item) {
		const dialogRef = this.dialog.open(ConfirmationModal, {
			width: '300px',
			data: {
				message: "Deseja realmente não aprovar o(a) " + item.Name + " para participar da marca?",
				confirmMessage: "Sim",
				cancelMessage: "Não"
			}
		});

		dialogRef.afterClosed().subscribe(result => { });
	}

	public onChangeComponentButtonClicked(componentName) {
		var component;
		
		if(componentName === 'calendario') {
			component = CalendarioComponent;
		} else if(componentName === 'lance/lances-recebidos') {
			component = LancesRecebidosComponent;
		}

		const dialogRef = this.dialog.open(component, {
			width: '1000px',
			data: {brandId: this.brandId, type: 'OPENING'}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (!result) return;
			this.mudaComponente.emit(result);
		});
	}

	public onEditButtonClicked() {

	}
}