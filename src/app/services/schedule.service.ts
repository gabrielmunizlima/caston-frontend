import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { PageSchedule } from 'src/models/event/page-schedule';
import { environment } from 'src/environments/environment';
import { Schedule } from 'src/models/event/schedule';

@Injectable({
    providedIn: 'root'
})
export class ScheduleService {

    private baseUrl = 'http://localhost:8091';

    private scheduleUrl = this.baseUrl + '/v1/schedules';

    private defaultApiUrl = environment.brandApiUrl + 'v1/schedules/';

    /**
     * getById
     */
    public getById(id: number) {
        this.http.get<any>(this.defaultApiUrl + '/' + id).subscribe(data => {
            return data;
        });
    }

    /**
     * getEvents
     */
    public getEvents(id: number) {
        this.http.get<any>(this.defaultApiUrl + '/' + id + '/events?size=100').subscribe(data => {
            return data;
        });
    }

    httpOptions = {
        headers: new HttpHeaders(
            { 'Content-Type': 'application/hal+json', 'Access-Control-Allow-Origin': '*' }
        )
    };

    constructor(private http: HttpClient) { }

    async findScheduleAvailable(dtInit, dtFinal): Promise<PageSchedule> {
        const url = `${this.scheduleUrl}/availables/?dtinit=${dtInit}&dtfinal=${dtFinal}&size=100`;

        return this.http.get<PageSchedule>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async findScheduleByBrandId(brandId: number): Promise<PageSchedule> {
        const url = `${this.scheduleUrl}/brand/${brandId}?size=100`;

        return this.http.get<PageSchedule>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async registerSchedule(schedule: Schedule): Promise<Schedule> {
        const url = `${this.scheduleUrl}`;
        
        return this.http.post<Schedule>(url, schedule, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            ).toPromise();
    }

    private handleError(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Erro ocorreu no lado do client
            errorMessage = error.error.message;
        } else {
            // Erro ocorreu no lado do servidor
            errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }
}
