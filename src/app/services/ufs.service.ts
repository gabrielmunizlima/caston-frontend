import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UfsService {

  constructor(private http: HttpClient) { }

  /**
   * getUfs
   */
  public getAll() {
    this.http.get<any>(environment.eventApiUrl + 'v1/ufs?size=100').subscribe(data => {
        return data;
    });
  }
}
