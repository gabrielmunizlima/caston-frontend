import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    public idUser: string;

    public type: string;
    
    constructor(private cookieService: CookieService) { 
        this.idUser = cookieService.get('idUser');
        this.type = cookieService.get('typeUser');
    }

    public Login(idUser: string, password: string) {
        this.idUser = idUser;
        
        // TO-DO : Implementar Login
        if (idUser == "robertosantos@gmail.com") {
            this.type = 'A';
        } else if (idUser == "contratante") {
            this.type = 'C';
        }

        this.cookieService.set('idUser', idUser);
        this.cookieService.set('typeUser', this.type);
    }
}

