import { TestBed } from '@angular/core/testing';

import { UfsService } from './ufs.service';

describe('UfsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UfsService = TestBed.get(UfsService);
    expect(service).toBeTruthy();
  });
});
