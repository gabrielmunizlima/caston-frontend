import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Event } from 'src/models/event/Event';
import { EventPlace } from 'src/models/event/eventPlace';


@Injectable({
	providedIn: 'root'
})
export class EventPlaceService {

	constructor(private http: HttpClient) { }

	private defaultApiUrl = environment.eventApiUrl + 'v1/event-places/';
	
	/**
	   * getEventsByOwnerHirer
	   */
	public async getByOwnerHirer(idOwnerHirer: number): Promise<EventPlace[]> {
		var data = await this.http.get<any>(this.defaultApiUrl + 'owner-hirer/' + idOwnerHirer + '?size=100').toPromise();
        const value = data._embedded.eventPlaceList;
        if (!value) return [];
		return value;
	}

	/**
	 * getById
	 */
	public getById(id: number) {
		this.http.get<any>(this.defaultApiUrl + '/' + id).subscribe(data => {
			return data;
		});
	}

	/**
	 * update
	 */
	public update(event: Event) {
		const body = event;
		this.http.put<any>(this.defaultApiUrl, body)
			.subscribe(data => { return data; });
	}

	/**
	 * save
	 */
	public async save(event: EventPlace) {
		var result = this.http.post<any>(this.defaultApiUrl, event).toPromise();
	}
}
