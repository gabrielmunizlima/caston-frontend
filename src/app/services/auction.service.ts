import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Offer } from 'src/models/auction/offer';
import { Auction } from 'src/models/auction/auction';
import { PageAuction } from 'src/models/auction/page-auction';

@Injectable({
    providedIn: 'root'
})
export class AuctionService {

    private baseUrl = 'http://localhost:8093';

    private auctionUrl = this.baseUrl + '/v1/auction';

    httpOptions = {
        headers: new HttpHeaders(
            { 'Content-Type': 'application/hal+json', 'Access-Control-Allow-Origin': '*' }
        )
    };

    constructor(private http: HttpClient) { }

    async findAuctionById(auctionId: number): Promise<Auction> {
        const url = `${this.auctionUrl}/${auctionId}`;
        
        return this.http.get<Auction>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async findAuctionsByBrandId(brandId: number): Promise<Array<Auction>> {
        const url = `${this.auctionUrl}/brand/${brandId}?size=100`;
        
        return this.http.get<Array<Auction>>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async findAuctionsByScheduleId(scheduleId: number): Promise<PageAuction> {
        const url = `${this.auctionUrl}/schedule/${scheduleId}?size=100`;
        
        return this.http.get<PageAuction>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async getMaxOfferByScheduleId(scheduleId: number): Promise<Offer> {
        const url = `${this.auctionUrl}/schedule/${scheduleId}/maxoffer`;
        
        return this.http.get<Offer>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async getMaxOfferByEventIdAndScheduleId(eventId: number, scheduleId: number): Promise<Offer> {
        const url = `${this.auctionUrl}/event/${eventId}/schedule/${scheduleId}/maxoffer`;
        
        return this.http.get<Offer>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async findAuctionByEventId(eventId: number): Promise<Auction> {
        const url = `${this.auctionUrl}/event/${eventId}`;
        
        return this.http.get<Auction>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async findAuctionByEventIdAndScheduleId(eventId: number, scheduleId: number): Promise<Auction> {
        const url = `${this.auctionUrl}/event/${eventId}/schedule/${scheduleId}`;
        
        return this.http.get<Auction>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async registerAuction(auction: Auction): Promise<Auction> {
        const url = `${this.auctionUrl}/register`;
        
        return this.http.post<Auction>(url, auction, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            ).toPromise();
    }

    async registerOffer(auctionId: number, offer: Offer): Promise<Auction> {
        const url = `${this.auctionUrl}/${auctionId}/offer`;
        
        return this.http.post<Auction>(url, offer, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            ).toPromise();
    }

    async updateOffer(auctionId: number, offer: Offer): Promise<Auction> {
        const url = `${this.auctionUrl}/${auctionId}/offer/update`;
        
        return this.http.post<Auction>(url, offer, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            ).toPromise();
    }

    private handleError(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Erro ocorreu no lado do client
            errorMessage = error.error.message;
        } else {
            // Erro ocorreu no lado do servidor
            errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }
}