import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Event } from 'src/models/event/Event';
import { environment } from 'src/environments/environment';
import { PageEvent } from 'src/models/event/page-event';

@Injectable({
    providedIn: 'root'
})
export class EventService {

    private baseUrl = 'http://localhost:8091';

    private eventUrl = this.baseUrl + '/v1/events';

    httpOptions = {
        headers: new HttpHeaders(
            { 'Content-Type': 'application/hal+json', 'Access-Control-Allow-Origin': '*' }
        )
    };

    constructor(private http: HttpClient) { }

    private defaultRoute = environment.eventApiUrl + 'v1/events/';

    async findAllEventsByBrandId(brandId: number): Promise<PageEvent> {
        const url = `${this.eventUrl}/brand/${brandId}?size=100`;

        return this.http.get<PageEvent>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async findEventById(eventId: number): Promise<Event> {
        const url = `${this.eventUrl}/${eventId}`;

        return this.http.get<Event>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    /**
     * getEventsByOwnerHirer
     */
    public async getEventsByOwnerHirer(idOwnerHirer: number): Promise<Event[]> {
        var data = await this.http.get<any>(this.defaultRoute + 'owner-hirer/' + idOwnerHirer + '?size=100').toPromise();
        const value = data._embedded.eventList;
        if (!value) return [];
        return value;
    }

    /**
     * cancel - /v1/events/{id}/cancel
     */
    public cancel(idEvent) {
        const body = {};
        this.http.put<any>(this.defaultRoute + idEvent + '/cancel', body)
            .subscribe(data => { return data });
    }

    /**
     * getById - /v1/events/{id}
     */
    public getById(idEvent: number) {
        this.http.get<any>(this.defaultRoute + idEvent).subscribe(data => {
            return data;
        });
    }

    /**
	 * save
	 */
	public async save(event: Event) {
		var result = this.http.post<any>(this.defaultRoute, event).toPromise();
	}

    private handleError(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Erro ocorreu no lado do client
            errorMessage = error.error.message;
        } else {
            // Erro ocorreu no lado do servidor
            errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }
}