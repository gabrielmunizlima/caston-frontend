import { TestBed } from '@angular/core/testing';

import { EventPlaceService } from './event-place.service';

describe('EventPlaceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventPlaceService = TestBed.get(EventPlaceService);
    expect(service).toBeTruthy();
  });
});
