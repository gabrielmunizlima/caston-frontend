import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class UtilitiesService {

	static isValidDate(d) {
		if (!d || d.length < 1) {
			return false;
		}
		const date = new Date(d);

		return date instanceof Date && !isNaN(this.convertDate(date).getTime());
	}

	static isValidTime(t) {
		if (!t || t.length < 1) {
			return false;
		}

		const timeReg = /^([0-1][0-9]|2[0-3]):([0-5][0-9])$/;
		return t.match(timeReg);
	}

	static convertDate(d) {
		const doo = new Date(d);
		return new Date( doo.getTime() - doo.getTimezoneOffset() * -60000 );
	}

	constructor(private http: HttpClient) { }

	async getImage(): Promise<IImage> {
		return await this.http.get<IImage>(
			'https://api.pexels.com/v1/search?query=music&per_page=10&page=1', { headers: { Authorization: '563492ad6f917000010000019b4bcffdefa840a69c7ab0074b998663' } }).toPromise();
	}
}

interface IImage {
	total_results: number;
	page: number;
	per_page: number;
	photos: {
		id: number;
		width: number;
		height: number;
		url: string;
		photographer: string;
		photographer_url: string;
		photographer_id: number;
		src: {
			original: string;
			large2x: string;
			large: string;
			medium: string;
			small: string;
			portrait: string;
			landscape: string;
			tiny: string;
		};
	}[];
	next_page: string;
}

