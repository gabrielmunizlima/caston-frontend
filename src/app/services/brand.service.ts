import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Brand } from 'src/models/brand/brand';

@Injectable({
    providedIn: 'root'
})
export class BrandService {

    private baseUrl = 'http://localhost:8092';

    private brandUrl = this.baseUrl + '/v1/brands';

    httpOptions = {
        headers: new HttpHeaders(
            { 'Content-Type': 'application/hal+json', 'Access-Control-Allow-Origin': '*' }
        )
    };

    constructor(private http: HttpClient) { }

    async findAllBrands(): Promise<Brand[]> {
        const url = `${this.brandUrl}?size=100`;
        var data = await this.http.get<any>(url, this.httpOptions).toPromise();
        const value = data._embedded.brandList;
        if (!value) return [];
        return value;
    }

    async findBrandById(brandId: number): Promise<Brand> {
        const url = `${this.brandUrl}/${brandId}`;

        return this.http.get<Brand>(url, this.httpOptions)
            .pipe(
                retry(2),
                catchError(this.handleError)
            ).toPromise();
    }

    async findBrandsByOwner(ownerId: number): Promise<Brand[]> {
        const url = `${this.brandUrl}/owner/${ownerId}?size=100`;
        var data = await this.http.get<any>(url, this.httpOptions).toPromise();
        const value = data._embedded.brandList;
        if (!value) return [];
        return value;
            
    }

    private handleError(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Erro ocorreu no lado do client
            errorMessage = error.error.message;
        } else {
            // Erro ocorreu no lado do servidor
            errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }
}