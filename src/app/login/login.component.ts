import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';
import { UtilitiesService } from '../services/utilities.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	imageUrl: any;

	Usuario: string;
	Senha: string;

	constructor(private utilities: UtilitiesService, private loginService: LoginService, private router: Router) {}

	ngOnInit() {
		this.Usuario = "robertosantos@gmail.com";
		this.Senha = "123456";
		this.getImagem();
	}

	async getImagem() {
		const image = await this.utilities.getImage();
		const index = Math.floor(Math.random() * (10));
		this.imageUrl = image.photos[index].src.portrait;
	}

	logar() {
		if ((this.Usuario === 'robertosantos@gmail.com' || this.Usuario === 'contratante') && this.Senha === '123456') {
			this.loginService.Login(this.Usuario, this.Senha);
			this.router.navigate(['/main']);
		}/*# TODO apresentar mensagem de erro caso ocorra erro / alterar para integração com as apis
		*/
	}

	cadastrar() {
		this.router.navigate(['/signup']);
	}

}
