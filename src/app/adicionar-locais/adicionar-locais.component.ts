import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { NotifierService } from 'angular-notifier';
import { Address } from 'src/models/commons/Address';
import { EventPlace } from 'src/models/event/eventPlace';
import { EventPlaceService } from '../services/event-place.service';

@Component({
  selector: 'app-adicionar-locais',
  templateUrl: './adicionar-locais.component.html',
  styleUrls: ['./adicionar-locais.component.scss']
})
export class AdicionarLocaisComponent implements OnInit {
  
  constructor(public dialogRef: MatDialogRef<AdicionarLocaisComponent>, private notifierService: NotifierService, private eventPlaceService: EventPlaceService) { 
    this.eventPlace = new EventPlace();
    this.eventPlace.address = new Address();
  }

  public eventPlace: EventPlace;

  ngOnInit() {
  }

  onAddEventPlaceButtonClicked() {
    this.eventPlaceService.save(this.eventPlace);
		this.notifierService.notify('success', 'Local cadastrado com sucesso!');
  }

  public onCloseButtonClick(): void {
		this.dialogRef.close();
	}
}
