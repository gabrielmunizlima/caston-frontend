import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarLocaisComponent } from './adicionar-locais.component';

describe('AdicionarLocaisComponent', () => {
  let component: AdicionarLocaisComponent;
  let fixture: ComponentFixture<AdicionarLocaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarLocaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarLocaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
