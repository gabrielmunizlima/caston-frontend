import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinhasMarcasComponent } from './minhas-marcas.component';

describe('MinhasMarcasComponent', () => {
  let component: MinhasMarcasComponent;
  let fixture: ComponentFixture<MinhasMarcasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinhasMarcasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinhasMarcasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
