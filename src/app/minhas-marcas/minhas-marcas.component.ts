import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';
import { MinhasMarcasDetalhes } from '../minhas-marcas-detalhes/minhas-marcas-detalhes';
import { BrandService } from 'src/app/services/brand.service';
import { Brand } from 'src/models/brand/brand';

export interface DialogData {
	animal: string;
	name: string;
}

@Component({
	selector: 'app-minhas-marcas',
	templateUrl: './minhas-marcas.component.html',
	styleUrls: ['./minhas-marcas.component.scss']
})
export class MinhasMarcasComponent implements OnInit {

	@Output() mudaComponente = new EventEmitter();

	displayedColumns: string[] = ['Seleciona', 'Nome', 'Nº Participantes', 'Detalhes'];
	dataSource = new MatTableDataSource();
	selection = new SelectionModel<any>(true, []);
	expandedElement: null;

	brands: Brand[];

	constructor(public dialog: MatDialog, private brandService: BrandService) { }

	ngOnInit() {
		this.lista();
	}

	public async lista() {
		//#TODO obter id do usuário de sessão
		this.brands = await this.brandService.findBrandsByOwner(6);
		this.dataSource = new MatTableDataSource(this.brands);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/** The label for the checkbox on the passed row */
	checkboxLabel(row): string {
		if (!row) {
			return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
	}

	adicionar() {
		this.mudaComponente.emit('marcas/adicionar');
	}

	onOpenButtonClick(element: Brand): void {
		const dialogRef = this.dialog.open(MinhasMarcasDetalhes, {
			width: '800px',
			data: {brandId: element.id, type: 'OPENING'}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (!result) return;
			this.mudaComponente.emit(result);
		});
	}

	onEditButtonClick(element: Brand): void {
		const dialogRef = this.dialog.open(MinhasMarcasDetalhes, {
			width: '800px',
			data: {brandId: element.id, type: 'EDITING'}
			
		});

		dialogRef.afterClosed().subscribe(result => {
			if (!result) return;
			this.mudaComponente.emit(result);
		});
	}
}
