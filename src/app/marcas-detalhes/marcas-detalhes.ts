import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationModal } from '../confirmation-modal/confirmation-modal';
import { Brand } from 'src/models/brand/brand';

@Component({
	selector: 'marcas-detalhes',
	templateUrl: 'marcas-detalhes.html',
	styleUrls: ['./marcas-detalhes.scss']

})
export class MarcasDetalhes {

	displayedColumns: string[];
	dataSource = new MatTableDataSource();
	selection = new SelectionModel<any>(true, []);

	brand: Brand;
	showButtons: boolean;
	genres: string;

	constructor(
		public dialog: MatDialog,
		public dialogRef: MatDialogRef<MarcasDetalhes>,
		@Inject(MAT_DIALOG_DATA) public data: IData
	) {
		this.brand = data.brand;
		this.showButtons = data.showButtons;
		this.genres = this.brand.genres.map(_ => _.name).join();

		if (this.showButtons) this.displayedColumns = ['Nome', 'Email', 'Opções'];
		else this.displayedColumns = ['Nome', 'Email'];
		this.dataSource = new MatTableDataSource(this.brand.members);
	}

	public onAskToJoinButtonClick(): void {
		const dialogRef = this.dialog.open(ConfirmationModal, {
			width: '300px',
			data: {
				message: "Deseja solicitar para participar da marca " + this.brand.name + " ?",
				confirmMessage: "Sim",
				cancelMessage: "Não"
			}
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}
}

interface IData {
	brand: Brand,
	showButtons: boolean
}