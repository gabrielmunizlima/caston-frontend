import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NotifierService } from 'angular-notifier';
import { map } from 'jquery';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';
import { UtilitiesService } from '../services/utilities.service';

@Component({
  selector: 'app-adiciona-marca',
  templateUrl: './adiciona-marca.component.html',
  styleUrls: ['./adiciona-marca.component.scss']
})
export class AdicionaMarcaComponent implements OnInit {

  Nome: string;
  Descricao: string;
  DataCriacao: string;
  Cpf: string;

  Integrantes = [];
  Generos = [{ id: 1, label: 'Rock' },
  { id: 2, label: 'MPB' },
  { id: 3, label: 'Samba' },
  { id: 4, label: 'Pagode' },
  { id: 5, label: 'Pop' }];
  
  ParticipacaoIntegrante: string;
  FuncaoIntegrante: string;
  NomeIntegrante: string;

  constructor() {
  }

  ngOnInit() {
  }

  adicionarIntegrante() {
		this.Integrantes.push({
			Nome: this.NomeIntegrante,
			Funcao: this.FuncaoIntegrante,
			Participacao: this.ParticipacaoIntegrante
		});

		this.ParticipacaoIntegrante = '';
		this.FuncaoIntegrante = '';
		this.NomeIntegrante = '';
	}

  deleteItem(item) {
    this.Integrantes.splice(item, 1);
  }

}
