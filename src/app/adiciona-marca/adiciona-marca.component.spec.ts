import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionaMarcaComponent } from './adiciona-marca.component';

describe('AdicionaMarcaComponent', () => {
  let component: AdicionaMarcaComponent;
  let fixture: ComponentFixture<AdicionaMarcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionaMarcaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionaMarcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
