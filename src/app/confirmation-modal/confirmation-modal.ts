import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Band } from '../Interfaces/Band';

@Component({
	selector: 'confirmation-modal',
	templateUrl: 'confirmation-modal.html',
	styleUrls: ['./confirmation-modal.scss']

})
export class ConfirmationModal {

	public Message: string = "";
	public ConfirmMessage: string = "";
	public CancelMessage: string = "";

	constructor(@Inject(MAT_DIALOG_DATA) public data: { message: string, confirmMessage: string, cancelMessage: string }) {
		this.Message = data.message;
		this.ConfirmMessage = data.confirmMessage;
		this.CancelMessage = data.cancelMessage;
	}
}