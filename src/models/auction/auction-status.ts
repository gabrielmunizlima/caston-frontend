export enum AuctionStatus {
    SENT,
    ACCEPT,
    REPROVED,
    CANCELED,
    UNDEFINED
}
