import { Auction } from './auction';

export class PageAuction {
	_embedded: AuctionList;
}

export class AuctionList {
    auctionList: Array<Auction>;
}