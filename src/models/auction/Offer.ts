export class Offer {
	id: number;
	value: number;
	accept: boolean;
	dtHrSent: Date;
	dtHrUpdate: Date;
}