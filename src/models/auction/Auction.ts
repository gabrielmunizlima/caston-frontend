import {Offer} from './offer';
import { Event } from 'src/models/event/Event';
import {AuctionStatus} from './auction-status';

export class Auction {
	id: number;
	brandId: number;
	scheduleId: number;
	eventId: number;
	event: Event;
	qtdOffers: number;
	auctionStatus: AuctionStatus = AuctionStatus.UNDEFINED;
	offers: Array<Offer>;
	lastOffer: Offer;

	constructor() {
		this.event = new Event();
	}
}
