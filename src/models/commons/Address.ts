export class Address {
	id: number;
	zipcode: string;
	number: number;
	complement: string;
	street: string;
	city: string;
	state: string;
	country: string;

	static getMocked() {
		const endereco = new Address();
		endereco.zipcode = '01311-000';
		endereco.number = 1106;
		endereco.complement = 'Primeiro Andar';
		endereco.street = 'Avenida Paulista';
		endereco.city = 'São Paulo';
		endereco.state = 'SP';
		endereco.country = 'Brasil'

		return endereco;
	}
}
