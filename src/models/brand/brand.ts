import {Artista} from './artista';
import {Genre} from './genre';
import {Set} from './portfolio';
import {Score} from './score';

export class Brand {
	id: number;
	name: string;
	scheduleId: number;
	members: Array<Artista>;
	genres: Array<Genre>;
	setList: Array<Set>;
	score: Score;
	principalGenre: string;
	distanceEventKm: number;
	minimumOffer: number;
	owner: Artista


	static getMocked() {
		const brand = new Brand();
		brand.id = 2;
		brand.name = 'João do Pop';
		brand.members = Array.of(Artista.getMocked());
		brand.genres = Array.of(Genre.getMocked());
		brand.setList = Array.of(Set.getMocked());
		brand.score = Score.getMocked();
		brand.principalGenre = 'Rock';
		brand.distanceEventKm = 20;
		brand.minimumOffer = 150.0;		 

		return brand;
	}
}
