export class Score {
	id: number;
	scorePoint: number;
	qtdeEvents: number;

	static getMocked() {
		const score = new Score();
		score.id = 1;
		score.scorePoint = 300;
		score.qtdeEvents = 50;

		return score;
	}
}
