import {Address} from '../commons/Address';

export class Usuario {
	name: string;
	documento: string;
	endereco: Address;
	email: string;
	type: String
}
