export class Set {
	musicName: string;
	interprete: string;
	authorial: boolean;
	duration: string;
	urlPreview: string;

	static getMocked() {
		const portfolio = new Set();
		portfolio.musicName = 'Take On Me';
		portfolio.interprete = 'a-ha';
		portfolio.authorial = false;
		portfolio.duration = '4:03';
		portfolio.urlPreview = 'https://www.youtube.com/watch?v=djV11Xbc914';

		return portfolio;
	}
}
