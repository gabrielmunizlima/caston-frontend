export class Genre {
	id: number;
	name: string;

	static getMocked() {
		const genero = new Genre();
		genero.id = 1;
		genero.name = 'Pop';

		return genero;
	}
}
