import {Score} from './score';
import {Usuario} from './usuario';
import {Address} from '../commons/Address';

export class Contratante extends Usuario {
	score: Score;

	static getMocked() {
		const contratante = new Contratante();
		contratante.name = 'City Bank Eventos';
		contratante.documento = '52.995.354/0001-17';
		contratante.endereco = Address.getMocked();
		contratante.email = 'eventos@citybank.com';
		contratante.score = Score.getMocked();

		return contratante;
	}
}
