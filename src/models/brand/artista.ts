import {Usuario} from './usuario';
import {Address} from '../commons/Address';

export class Artista extends Usuario {
	dtNasc: string;

	static getMocked() {
		const artista = new Artista();
		artista.name = 'João Carlos';
		artista.documento = '123.456.789-09';
		artista.endereco = Address.getMocked();
		artista.email = 'joao.carlos@gmail.com';
		artista.dtNasc = '12/05/1980';

		return artista;
	}
}
