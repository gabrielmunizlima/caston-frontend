import {Contratante} from '../brand/contratante';
import {Brand} from '../brand/brand';
import {Event} from '../event/Event';
import {StatusPagamento} from './statuspagamento';

export class Pagamento {
	contratante: Contratante;
	marca: Brand;
	evento: Event;
	valorTotal: number;
	status: StatusPagamento;
}
