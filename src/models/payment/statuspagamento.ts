export enum StatusPagamento {
	ABERTO = 'O',
	PROCESSANDO = 'P',
	CANCELADO = 'C',
	APROVADO = 'A'
}
