import {Pagamento} from './pagamento';

export class Creditos {
	pagamento: Pagamento;
	valorRecebido: number;
	percentualAplicado: number;
	taxaServico: number;
	valorCredito: number;
	dtRecebido: string;
}
