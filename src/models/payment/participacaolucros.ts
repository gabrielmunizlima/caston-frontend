import {Artista} from '../brand/artista';
import {Brand} from '../brand/brand';

export class ParticipacaoLucros {
	artista: Artista;
	marca: Brand;
	percentual: number;
}
