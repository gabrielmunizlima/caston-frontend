import {TipoDebito} from './tipodebito';

export class Debitos {
	valorDebito: number;
	tipoDebito: TipoDebito;
	dtEvento: string;
}
