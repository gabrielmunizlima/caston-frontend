import {Usuario} from '../brand/usuario';
import {Creditos} from './creditos';
import {Debitos} from './debitos';

export class Carteira {
	usuario: Usuario;
	saldo: number;
	creditos: Array<Creditos>;
	debitos: Array<Debitos>;
}
