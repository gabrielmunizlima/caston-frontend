import {Address} from '../commons/Address';
import {Contratante} from '../brand/contratante';

export class EventPlace {
	id: number;
	name: string;
	address: Address;
	ownerHirerId: number;

	static getMocked() {
		const localEvento = new EventPlace();
		localEvento.name = 'City Bank Hall';
		localEvento.address = Address.getMocked();

		return localEvento;
	}
}
