export enum EventStatus {
	OPEN = 'em aberto',
	READY = 'pronto',
	INPROGRESS = 'em andamento',
	REALIZED = 'já realizado',
	CANCELLED = 'cancelado',
	FREE_TIME = 'horário livre'	
}
