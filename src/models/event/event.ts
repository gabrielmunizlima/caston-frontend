import {EventPlace} from './eventPlace';
import {Avaliacao} from './avaliacao';
import {Brand} from '../brand/brand';
import {Offer} from '../auction/offer';
import {EventStatus} from './eventstatus';
import {Schedule} from './schedule';

export class Event {
	id: number;
	name: string;
	dthrInit: Date;
	dthrFinal: Date;
	ratingArtist: Avaliacao;
	ratingHirer: Avaliacao;
	marcaContratada: Brand;
	cache: Offer;
	statusEvent: EventStatus;
	bookingBy: Schedule;
	eventplace: EventPlace;
}
