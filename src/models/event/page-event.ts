import { Event } from './Event';

export class PageEvent {
	_embedded: EventList;
}

export class EventList {
    eventList: Array<Event>;
}