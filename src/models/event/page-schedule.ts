import { Schedule } from './schedule';

export class PageSchedule {
	_embedded: ScheduleList;
}

export class ScheduleList {
    scheduleList: Array<Schedule>;
}