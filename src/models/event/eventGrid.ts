
export class EventGrid {
  id: string;
  title: string;
  start: string;
  end: string;
  status: string;

  constructor(id, title, start, end, status) {
    this.id = id;
    this.title = title;
    this.start = start;
    this.end = end;
    this.status = status;
  }
}
