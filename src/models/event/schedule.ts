export class Schedule {
	id: number;
	brandId: number;
	dthrInit: Date;
	dthrFinal: Date;
	available: boolean;
 }
